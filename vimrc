set nocompatible

execute pathogen#infect()

" Colors
colorscheme tomorrow-night

" backups
set nobackup
set noswapfile

" Settings
syntax on
set number
set cursorline
set colorcolumn=80
set nowrap
set ruler

" Display
set t_Co=256
set laststatus=2

" Tabs
set tabstop=2
set shiftwidth=2
set expandtab

" Syntax
filetype plugin indent on

" bindings
let mapleader = "\\"
nmap <leader>w :w!<CR>
nmap <leader>q :q!<CR>
nmap <leader>vv :vsplit<CR>
nmap <leader>hh :split<CR>
nmap <leader>g :Gcommit %<CR>
nmap <leader>gs :Gstatus<CR>
nmap <leader>gg :Git add %<CR>
nmap <leader>gp :Git push<CR>
nmap <leader>r :source ~/.vimrc<CR>
nmap <leader>b :Bundle<CR>

" ctrlp
let g:ctrlp_custom_ignore = {
  \ 'dir':  '\v[\/](\.(git|hg|svn)|coverage|node_modules|vendor\/bundle)$',
  \ 'file': '\v\.(exe|so|dll)$',
  \ 'link': 'some_bad_symbolic_links',
  \ }

" Reload vimrc automatically
augroup myvimrc
	au!
	au BufWritePost .vimrc,_vimrc,vimrc,.gvimrc,_gvimrc,gvimrc so $MYVIMRC | if has('gui_running') | so $MYGVIMRC | endif
augroup END
