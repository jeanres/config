ln -s ~/config/vimrc ~/.vimrc
ln -s ~/config/tmux.conf ~/.tmux.conf
ln -s ~/config/gemrc ~/.gemrc
ln -s ~/config/railsrc ~/.railsrc
ln -s ~/config/gitignore ~/.gitignore
ln -s ~/config/vim ~/.vim
ln -s ~/config/gitconfig ~/.gitconfig

ln -s ~/Dropbox/private/ssh/ ~/.ssh

cd ~/config/ && git pull && git submodule init && git submodule update && git submodule status

